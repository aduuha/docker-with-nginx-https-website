# Docker with NGINX HTTPS website

# Веб сервер на Nginx с использованием letsencrypt сертификата в Docker Compose контейнере.
# 
# На Виртуальной машине устанавил Docker и Docker Compose
# Простой конфиг docker-compose.yaml для запуска nginx и проброса в него index.html из папки на сервере
# Так же локально наш сайт будет доступен по порту 8888
#
# version: '3'
# services:
#   nginx:
#     image: nginx:latest
#     ports:
#       - "8888:80"
#     volumes:
#       - ./html:/usr/share/nginx/html
#       - ./cache:/var/cache/nginx
#
# Зарегестрировал домен arthutor.ru и все остальные задачи выполнил на хостинге
# Скрипты в папках, комииты есть в самих скриптах 
# Использую два взаимо зависимых контейнера с nginx и certbot
# Создаю локальные директории для хранения конфигурационных файлов nginx, certbot и самого сайта index.html в папке ./Data
# Для получения сертификатов я использовал найденный скрипт init-letsencrypt.sh отредактированный под мой хост
# Скрипт создает фиктивный сертификат, запускает nginx, удаляет фиктивный и запрашивает letsencrypt сертификаты
# curl -L https://raw.githubusercontent.com/wmnnd/nginx-certbot/master/init-letsencrypt.sh > init-letsencrypt.sh
# chmod +x init-letsencrypt.sh и запускаю ./init-letsencrypt.sh
# docker-compose up и смотрим лог ошибок или  docker-compose up -d 
# для подключения к командной строке контейнера docker-compose exec nginx bash
# просмотр конфигурации nginx в контейнере docker-compose exec nginx cat /etc/nginx/conf.d/default.conf
#
# Настроил proxy pass на https://www.cbr.ru/scripts/XML_daily.asp
# На адрес https://arthutor.ru/cbr
# Так же в nginx app.conf настроил что, при доступности ресурса всегда отдавались свежие данные, а при недоступности данные отдавались из кша. 
# Игнорируются заголовки no-cache и т.п., мешающие кэшированию. 
# Отдельным заголовком на ресурс передается реальный # IP адрес клиента
#
# В итоге на хостинге работает небольшой сайт-визитка arthutor.ru имеющий сертификат и обеспечивающий безопасное подключение
